# Published Referenced Entity

The Published referenced entity is a drupal module that implement the
specialized display format for fields that reference other entities. In Drupal,
entities are a fundamental concept and can represent various types of content,
such as nodes, users, taxonomy terms, etc.

By default, Drupal provides a set of field formatters for different field types.
However, in certain cases, you may want to display only published entities or
active users for fields that reference entities.

The module provides an administrative interface where you can configure the
settings for the field formatter. Once the module is enabled, you would be able
to select the field formatter for referenced entity fields on the Manage Display
configuration page of the entity type to which the field belongs. If you have
referenced entities that are not published, they will not be displayed when
rendering the entity reference field. This module provides the following
formatters for the nodes, users and taxonomy terms referenced entity field:

- Published Entity ID
- Published Entity Label
- Published Rendered Entity

For a full description of the module, visit the project page:
[project page](https://www.drupal.org/project/published_referenced_entity)

To submit bug reports and feature suggestions, or to track changes:
[issue queue](https://www.drupal.org/project/issues/published_referenced_entity)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the Published referenced entity module:
1. Go to the Extend page (/admin/modules) in your Drupal site.
1. Find the "Published referenced entity" module in the list and enable it.
1. Click the "Save configuration" button to apply the changes.
1. Go to the Manage Fields page for your content type:
1. Navigate to the Content Type configuration page for the content type where
   you want to add instructions to a field.
1. Add the referenced field for the node, user or taxonomy term entities
1. Click on the "Manage fields" tab for that content type.
1. Edit the field and configure the formatter. Select the Published Entity ID or
   Published Entity Label or Published Rendered Entity formatter and other
   configurations.
1. Click the "Save" button to save the field configuration after updating the
   formatters.
1. After making these changes, the referenced entities will not be rendered and
   displayed if they are not published or active.


## Maintainers

- [sujan shrestha](https://www.drupal.org/u/sujan-shrestha)
