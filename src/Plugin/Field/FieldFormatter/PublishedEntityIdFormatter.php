<?php

namespace Drupal\published_referenced_entity\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "published_entity_id",
 *   label = @Translation("Published Entity ID"),
 *   description = @Translation("Display the ID of the published referenced entities."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class PublishedEntityIdFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $getEntityTypeId = $entity->getEntityTypeId();

      switch ($getEntityTypeId) {
        case 'user':
          $isPublished = $entity->isActive();
          break;

        case 'node':
        case 'taxonomy_term':
          $isPublished = $entity->isPublished();
          break;

        default:
          $isPublished = TRUE;
          break;
      }

      if ($entity->id() && $isPublished) {
        $elements[$delta] = [
          '#plain_text' => $entity->id(),
          // Create a cache tag entry for the referenced entity. In the case
          // that the referenced entity is deleted, the cache for referring
          // entities must be cleared.
          '#cache' => [
            'tags' => $entity->getCacheTags(),
          ],
        ];
      }
    }

    return $elements;
  }

}
